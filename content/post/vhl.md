---
title: "Completed Virtual Hacking Labs"
hide: true
showdate: true
Date: "2021-05-21"
# tags: ["Certifications", "Cybersecurity"]
---

<p style="text-align:center">
  <img src="/png/certs/vhl-cert.png" width="700"/>
</p>

## Exam Overview

> Cost: Depends on subsciption purchased.

> Length: Depends on subsciption purchased.

> Number of required reports/rooted machines: 20

> [Virtual Hacking Labs](https://www.virtualhackinglabs.com/)

<object data="/pdf/certs/vhl-cert.png" type="application/pdf" width="100%" height="400"></object>


