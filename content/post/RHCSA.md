---
title: "Passed RHCSA exam!"
Date: "2021-03-23"
showdate: true
tags: ["Certifications", "Linux"]
---

<p style="text-align:center">
  <img src="/png/certs/rhcsa.png" width="700"/>
</p>

## Exam Overview

> Cost: $400

>       $200 for RHA students

> Length: 3 hours

> [RHCSA Exam Overview @redhat.com](https://www.redhat.com/en/services/training/ex200-red-hat-certified-system-administrator-rhcsa-exam?section=Overview)

## My Journey

I passed the RHCSA on March 23rd, 2021 with a score of 255/300! I wish I could say I passed the RHCSA exam on my first attempt, but it took me two attempts to officially be certified in system administration for RHEL 8.
This was the first industry certification I failed and had to retake. Before taking the RHCSA, I passed the [PE124 exam](https://www.redhat.com/en/services/training/pe124-preliminary-exam-red-hat-system-administration-I) on June 30, 2020.
I had been studying consistently since the end of January 2021 and was eager to take the exam again after my first failed attempt because I knew I could have passed but my lack of industry experience was something I realized was my weakest point when faced with a problem I never practiced or studied.
I felt prepared on my first attempt and believed I could have passed it if I was able to fix one issue which prevented some of my exam questions from being graded.
I can not go into detail about the RHCSA exam other than what is public knowledge because of an NDA and to maintain the integrity of the exam.
My first attempt at the RHCSA was on March 11th, 2021, and I got a 150/300 score. I have no shame in sharing my failures because from my experience, that's where I learn the most, and I hope my full experience can help someone more than just sharing my successes.

```
OBJECTIVE: SCORE
Understand and use essential tools: 75%
Operate running systems: 14%
Configure local storage: 0%
Create and configure file systems: 33%
Deploy, configure and maintain systems: 29%
Manage users and groups: 100%
Manage security: 100%
Manage containers: 0%
```

I tried to schedule my exam again for the week following my first attempt. However, it took a few days to receive my exam voucher after purchasing it, and the only open time slots to take the exam were around two weeks out, so I ended up retaking the exam 12 days from my first attempt.
After reviewing my objective breakdown scores and reflecting on my first attempt, I knew the areas I needed to work on. I felt even more prepared for my second attempt but knew while taking it, I still lacked experience with containers. When I got my object breakdown scores, I was a little disappointed because I did not improve on `Manage containers` and was in the 80 percentages for several categories. Nevertheless, I was still happy to pass and obtain the RHCSA.

```
OBJECTIVE: SCORE
Understand and use essential tools: 100%
Operate running systems: 86%
Configure local storage: 80%
Create and configure file systems: 83%
Deploy, configure and maintain systems: 86%
Manage users and groups: 100%
Manage security: 100%
Manage containers: 0%
```

I know I didn’t learn everything about Linux from my RHCSA journey, but I did gain solid skills on the fundamentals of Linux administration that are going to be valuable in my career.

## My Background
I am not someone who has years of industry experience or has been familiar with Linux for a long time. I have been using a Linux distribution as my main OS for my laptop since my Junior year of college, but I wouldn't consider myself a proficient Linux user after obtaining the PE124 or before studying for the RHCSA in late January. I got introduced to RHEL by being a Red Hat Student Ambassador and passed the PE124 on June 30, 2020. It essentially teaches about half of what is covered on the RHCSA. You can learn more about it on my other post talking about it. [PE124 Journey and Overview](/post/PE124.md)
The PE124 was a really good intermediate step for the RHCSA because it tests the less advanced objectives on the RHCSA. It is a good indicator if you're retaining what you're learning and if you need to change up your learning regime.
## How To Prepare

Since I am a Red Hat Student Ambassador, I was given training at no cost on Red Hat's learning platform for the RH124 and RH134 courses with a 3-month access to the labs for both courses. If you are an RHA student, you can receive access to RH124 and RH134 curriculum without the labs on the RHA portal from your instructor which is web-based. You can get labs for RH124 and RH134 from [infoseclearning](https://www.infoseclearning.com/virtual-labs) and [NDG](https://www.netdevgroup.com/). There is a cost for a subscription and the labs are connected to the RHA portal. 

For those on a budget and trying to access training at the lowest cost or no cost, I would recommend Sander Van Vugt's course or Youtube, Kadacoda, and rdbreak's lab environment.
> [Sander Van Vugt course](https://www.sandervanvugt.com/learn-rhel-8/) is on O'Reilly learning platform and costs $49/month for full access to his video course and book.

> [Computers, Security & Gadgets](https://www.youtube.com/c/ComputersSecurityGadgets/playlists) Youtube channel contains RHCSA playlists.

> [Katacoda](https://katacoda.com/rhel-labs/) RHEL labs contain a lab environment you can use at no cost.

> [rdbreak's](https://github.com/rdbreak/rhcsa8env) RHCSA 8 lab environment. I don't believe it has been updated to contain containers.

To pass the RHCSA, it's all about practice, practice, pratice. Going though each exam objective and making sure you understand it.

## Exam Day
I took the remote exam option and had to purchase an external webcamera to meet the requirements of having `One external webcam with at least a 1m cable`. You need to make sure you have two cameras and one of them needs to be an external camera. Once you register to take the RHCSA on a certain date, you will receive an email with more information on how to prepare for exam day.

The webcamera I purchased was a [Larmtek 1080p Full Hd Webcam](https://www.amazon.com/gp/product/B07P84DN2K/ref=ppx_yo_dt_b_asin_title_o08_s00?ie=UTF8&psc=1) on Amazon and it was under $30. The only problem I ran into with this camera is low-quality video capture on close up objects like my driver's license. I had to use my laptop camera instead to show my driver's license clearly. You will also need at least an 8GB USB to live boot into the exam environment where you will take the exam and can also run a system compatibility check.

## Next Steps
I plan to continue to learn more about system administration and Linux. Now that I am more competent with Linux, I would like to learn Windows system administration skills. I also plan to learn Ansible which is a configuration management tool used to automate system administration tasks for both Linux and Windows. I will also be working on helping RHA students pass the PE124 exam and the RHCSA. 