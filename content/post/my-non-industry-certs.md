---
title: "Collection of Course Completion Certifications"
showdate: false
Date: "2021-06-27"
tags: ["Certifications"]
---

# Summary

This post is a collection of courses I have taken  and have given me a certification. 
I have learned that a structured course to learn new technology and skills is a great way to accelerate my learning and get new ideas for projects.
Since I shifted my attention to learning about cybersecurity and technology after sophomore year in college, majority of the courses I got a certification for are cybersecurity related.

# Cybersecurity

TryHackMe is one of my favorite platforms to learn cybersecurity. The badge below gives an overview of my learning progress.
<script src="https://tryhackme.com/badge/222317"></script>

## Fundementals

### TryHackme Complete Beginner Path
<p style="text-align:center">
  <img src="/png/certs/THM-beginner-path.png" width="500"/>
</p>

### Security Blue Team - Intro to Network Analysis
<p style="text-align:center">
  <img src="/png/certs/intro-to-network-analysis.png" width="500"/>
</p>

### CodePath Cybersecurity Course - Web Vulnerabilities
<p style="text-align:center">
  <img src="/png/certs/codepath-cybersecurity.png" width="500"/>
</p>

### Codebashing Application Security and Secure Code Training
<p style="text-align:center">
  <img src="/png/certs/django-app-security.png" width="500"/>
</p>


### Splunk 7 Fundementals 
<p style="text-align:center">
  <img src="/png/certs/Splunk-7-Fundementals.png" width="500"/>
</p>


## Pentesting
<p style="text-align:center">
  <img src="/png/certs/vhl-cert.png" width="500"/>
</p>

# System Administration

I obtained my Red Hat Certified System Administrator (RHCSA) and you can learn more about it on my other post.

### Ansible
<p style="text-align:center">
  <img src="/png/certs/ansible-for-the-devops-beginner.png" width="500"/>
  <img src="/png/certs/ansible-complete-beginner.png" width="500"/>
</p>

### Windows System Administration
<p style="text-align:center">
  <img src="/png/certs/windows-2016-cert.png" width="500"/>
</p>