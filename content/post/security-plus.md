---
title: "Paased Security+"
hide: true
showdate: true
Date: "2021-04-03"
# tags: ["Certifications", "Cybersecurity"]
---

<p style="text-align:center">
  <img src="/png/certs/security+.png" width="700"/>
</p>

## Exam Overview

> Exam Cost: $370

>              $240 for students with .edu email

> Length: 90 minutes

> Max number of questions: 90

> [Security+ Details @comptia.com](https://www.comptia.org/certifications/security#examdetails)

## Exam Journey
I passed the CompTIA Security+ exam with a score of 760/900 on my first attempt! The passing score is 750 ...

<p style="margin-top:30px;text-align:center;">
  <img src="/memes/whew.jpeg" width="300" style="margin-right:10px;"/>
</p>

I decided to study for Security+ 501 vs 601 version since I was given free 501 study material with an exam voucher. Security+ 501 ends on July 31st, 2020 and will be replaced by 601. The exam formats are the same except that 601 contains updated material like IoT, cloud, and blockchain to name a few.

Since I was given an exam voucher, I wanted to take full advantage of it. I started studying around March 2021 and used videos from Jason Dion Secuirty+ course. Since I have been trying to learn Cybersecurity on my own and through Texas A&M Cybersecurity Club since my junior year in college, some of the material was a review. There were definitely a lot of topics and concepts I never learned and I was happy I spent the time to go through the course and learn what I didn't know. 

Learning the acronyms used for certain technologies, protocols, or just references to multiple words was very time-consuming. It was something I realized was important because I wouldn't be able to understand the questions if I don't know what an acronym means. I know there are several things I learned that I would probably not use in my career but think the overall topics covered gives a good holistic overview of cybersecurity. 

<object data="/pdf/certs/Security+Score Report.pdf" type="application/pdf" width="100%" height="400"></object>