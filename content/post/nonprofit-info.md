---
title: "Nonprofit - Next Generation Empowerment"
hide: true
date: "2020-12-31"
# tags: ["nextgenempower"]
---

# Mission Statement
> Our mission is to empower the younger generation to find a sense of purpose, build confidence, and grow as leaders by providing assistance to receive various opportunities to build their career and learn new skills. We aim for this mission to become a movement so the current generation that has been impacted by our efforts will have the same desire to help the next generation.

# Board Memebers
- Jonathan Saenz - Founder and President
- Arjun Lalith - Chief Innovation Officer
- Teddy Heinen - Chief Technology Officer
- Cecilla Weigman - Secretary and Treasurer
- David Saenz - Chief Strategy Officer

# Documents
## 501(c)(3) Approved
<object data="/pdf/nonprofit/irsApproval.pdf" type="application/pdf" width="100%" height="700"></object>

## Bylaws
<object data="/pdf/nonprofit/bylaws.pdf" type="application/pdf" width="100%" height="400"></object>

## Certificate of Filing
<object data="/pdf/nonprofit/certOfFilling.pdf" type="application/pdf" width="100%" height="400"></object>