---
title: "Passed AWS Solution Architect Associate"
hide: true
showdate: true
Date: "2021-06-10"
# tags: ["Certifications", "AWS"]
---

<p style="text-align:center">
  <img src="/png/certs/aws-saa.png" width="700"/>
</p>

## Exam Overview

> Exam Cost: $150

> Length: 130 minutes

> Number of questions: 65

> [AWS Certified Solutions Architect – Associate @aws.amazon.com](https://aws.amazon.com/certification/certified-solutions-architect-associate/)


I passed the AWS Certified Solutions Architect-Associate exam on my first attempt! I scored 779/1000 which is something I am not supper proud of but now that I have the exam, I can focus on building projects on AWS to gain hands-on experience.

<object data="/pdf/certs/AWS Certified Solutions Architect-Associate Report.pdf" type="application/pdf" width="100%" height="400"></object>