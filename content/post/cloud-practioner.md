---
title: "Passed AWS Cloud Practitioner!"
hide: true
showdate: true
Date: "2020-05-11"
# tags: ["Certifications", "AWS"]
---

<p style="text-align:center">
  <img src="/png/certs/aws-ccp.png" width="700"/>
</p

## Exam Overview

> Exam Cost: $100

> Length: 90 minutes

> Number of questions: 65

> [AWS Certified Cloud Practitioner Details @aws.amazon.com](https://aws.amazon.com/certification/certified-cloud-practitioner/)


I passed the AWS Certified Cloud Practitioner exam with a score of 908/1000 on my first attempt!

<object data="/pdf/certs/AWS Certified Cloud Practitioner Report.pdf" type="application/pdf" width="100%" height="400"></object>